export interface PokeApiPokemonResult {
  id: number;
  name: string;
  sprites: { front_default?: string; back_default?: string };
  types: [{ type: { name: string } }];
  stats: [{ base_stat: number; stat: { name: string } }];
}

export interface PokeApiResponse {
  count: number;
  next: string;
  previous: string;
  results: PokeApiPokemonResult[];
}
