export interface Pokemon {
  id: number;
  name: string;
  avatar?: string;
  isCaught: Boolean;
  types:[{type:{name:string}}];
  stats:[{base_stat:number, stat:{name:string}}];
}

