import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';
import { Router } from '@angular/router';
import { BrowserStorageService } from './services/browser-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'pokemon-trainer';
  constructor(
    private readonly authService: AuthService,
    private readonly storage: BrowserStorageService,
    private router: Router
  ) {}
  ngOnInit(): void {
    // Detects changes in localstorage
    // if there is no user in localStorage redirect user to login page
    if (window.addEventListener) {
      window.addEventListener('storage', () => {
        const trainer = this.storage.getLocal('trainer');
        if (trainer === null) {
          this.authService.logout();
        }
      });
    }
  }
  public tempLogout(): void {
    this.authService.logout();
  }
  get hasUser() {
    return this.authService.hasUser;
  }
}
