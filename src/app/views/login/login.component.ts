import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { PokemonTrainer } from '../../models/pokemon-trainer.model';
import { PokemonTrainerService } from '../../services/pokemon-trainer.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  constructor(
    private readonly pokemonTrainerService: PokemonTrainerService,
    private router: Router,
    private auth: AuthService
  ) {}
  isLoading = false;
  ngOnInit(): void {
    if (this.pokemonTrainerService.hasTrainer())
      this.router.navigateByUrl('catalogue');
    else {
    }
  }

  public onSubmit(createUser: NgForm): void {
    // Push the user onto assignment API
    const value = createUser.value;
    this.isLoading = true;
    this.auth.login(value.username);
  }
}
