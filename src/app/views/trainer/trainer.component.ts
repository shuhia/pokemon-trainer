import { Component, OnInit } from '@angular/core';
import { PokemonTrainerService } from '../../services/pokemon-trainer.service';
import { PokemonTrainer } from '../../models/pokemon-trainer.model';
import { BrowserStorageService } from 'src/app/services/browser-storage.service';
import { AuthService } from './../../services/auth.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.css'],
})
export class TrainerComponent implements OnInit {
  constructor(
    private readonly trainerService: PokemonTrainerService,
    private readonly storage: BrowserStorageService,
    private readonly authService: AuthService
  ) {}
  get trainer(): PokemonTrainer {
    return this.trainerService.pokemonTrainer();
  }

  ngOnInit(): void {
    // Redirects user to landing page if trainer has no name in localStorage
    if (window.addEventListener) {
      window.addEventListener('storage', () => {
        const trainer = this.storage.getLocal('trainer') as PokemonTrainer;
        if (!trainer?.username) {
          this.authService.logout();
        }
      });
    }
  }
}
