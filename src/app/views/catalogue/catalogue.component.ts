import { Component, OnInit } from '@angular/core';
import { PokemonService } from '../../services/pokemons.service';
@Component({
  selector: 'pokemon-catalogue',
  templateUrl: './catalogue.component.html',
})
export class CatalogueComponent implements OnInit {
  constructor(private readonly pokemonService: PokemonService) {}
  ngOnInit(): void {}

  get pokemonNames() {
    return this.pokemonService.pokemonNames();
  }
}
