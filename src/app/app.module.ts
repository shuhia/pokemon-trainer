import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { LoginComponent } from './views/login/login.component';
import { FormsModule } from '@angular/forms';
import { PokemonTableComponent } from './components/pokemon-table/pokemon-table.component';
import { CatalogueComponent } from './views/catalogue/catalogue.component';

import { TrainerComponent } from './views/trainer/trainer.component';
import { PokemonTableRowComponent } from './components/pokemon-table-row/pokemon-table-row.component';
import { PokemonModalComponent } from './components/pokemon-modal/pokemon-modal.component';
import { PokemonCardComponent } from './components/pokemon-card/pokemon-card.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PokemonTableComponent,
    TrainerComponent,
    CatalogueComponent,
    PokemonTableRowComponent,
    PokemonModalComponent,
    PokemonCardComponent
  ],
  imports: [BrowserModule, FormsModule, AppRoutingModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
