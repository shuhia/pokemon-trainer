import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PokemonTrainer } from './../models/pokemon-trainer.model';
import * as lodash from 'lodash';
/**
 * This class is used to handle API respons from Trainer API and provides data to view components
 */
export interface ApiResponse {
  id: number;
  username: string;
  pokemon: string[];
}

// A trainer object that represents a invalid trainer
const nullTrainer = {
  id: -1,
  username: '',
  pokemon: [],
};

// Makes sure that this object is a singleton.
@Injectable({
  providedIn: 'root',
})

// Used to get and provide contacts to Application
export class PokemonTrainerService {
  private _trainer: PokemonTrainer = this.loadInitialTrainer();

  // private _assignmentApiResponse?: AssignmentApiResponse;
  private _pokemonTrainers: PokemonTrainer[] = [];
  private _error: string = '';

  // DI - Dependency Injection
  constructor(private readonly http: HttpClient) {}

  // Pokemon Trainer methods
  public catchPokemon(name: string) {
    this.addPokemon(name);
  }

  public releasePokemon(name: string) {
    this.removePokemon(name);
  }

  /**
   * Fetches a specific trainer and also registers trainer if it does not exist.
   * @param username
   * @returns {promise}
   */
  public fetchTrainer(username: string) {
    const promise = new Promise((resolve, reject) => {
      const url = `https://noroff-assignment-api-shuhia.herokuapp.com/trainers?username=${username}`;
      const observer = this.http.get<ApiResponse[]>(url);
      observer.subscribe({
        next: (trainers) => {
          // Set to first trainer
          if (trainers.length > 0) {
            this.setTrainer(trainers[0]);

            console.log(`User ${this._trainer.username}, already exists.`);
            resolve(true);
          } else {
            // Register a new trainer
            this.registerTrainer(username).subscribe({
              complete: () => resolve(true),
            });
            console.log('User does not exist');
          }
        },
        error: (error) => {
          console.log(error);
          reject(error);
        },
        complete: () => {
          console.log('Completed fetchTrainer');
        },
      });
    });
    return promise;
  }

  // registers a new trainer
  public registerTrainer(username: string) {
    const url = `https://noroff-assignment-api-shuhia.herokuapp.com/trainers?username=${username}`;
    const body = JSON.stringify({
      username: username,
      pokemon: [],
    });
    const API_KEY = 'random';
    const options = {
      headers: {
        'X-API-Key': API_KEY,
        'Content-Type': 'application/json',
      },
    };
    const observer = this.http.post<ApiResponse>(url, body, options);
    observer.subscribe({
      next: (trainer) => {
        this.setTrainer(trainer);
        console.log(`registered new trainer ${this._trainer.username}`);
        console.log(trainer);
      },
      error: (error) => {
        console.log(error);
      },
      complete: () => {
        console.log('Completed registerTrainer');
      },
    });
    return observer;
  }

  /**
   * Updates trainer with trainer id and trainer pokemons
   * @param trainer
   */
  public updateTrainer(trainer: PokemonTrainer) {
    const url = `https://noroff-assignment-api-shuhia.herokuapp.com/trainers/${trainer.id}`;
    const body = JSON.stringify({ pokemon: trainer.pokemon });
    const API_KEY = 'random';
    const options = {
      headers: {
        'X-API-Key': API_KEY,
        'Content-Type': 'application/json',
      },
    };
    this.http.patch<PokemonTrainer>(url, body, options).subscribe({
      next: (updatedTrainer) => {
        if (!lodash.isEqual(updatedTrainer, trainer)) {
          throw new Error('Updated trainer is not equal to new trainer');
        }
      },
      error: (error) => {
        console.log(error);
      },
      complete: () => {
        console.log('Completed registerTrainer');
      },
    });
  }

  // GETTERS
  /**
   * Sets initial trainers
   * @returns
   */
  public loadInitialTrainer(): PokemonTrainer {
    return nullTrainer;
  }
  public pokemonTrainers(): PokemonTrainer[] {
    return this._pokemonTrainers;
  }

  public pokemonTrainer(): PokemonTrainer {
    return this._trainer;
  }
  public getPokemonNames() {
    return this._trainer.pokemon;
  }

  // SETTERS
  private setTrainer(trainer: ApiResponse) {
    this._trainer = trainer;
    this.saveTrainer();
  }

  public removePokemon(name: string) {
    this._trainer.pokemon = this._trainer.pokemon.filter(
      (prev) => prev !== name
    );
    this.saveTrainer();
  }

  private addPokemon(name: string): void {
    this._trainer?.pokemon.push(name);
    this.saveTrainer();
  }
  /**
   * Checks if trainer is not a null trainer
   * @returns null if trainer id is -1 or undefined else true
   */
  public hasTrainer(): Boolean {
    if (this._trainer.id === -1) return false;
    else {
      return true;
    }
  }
  /**
   * sets trainer to nullTrainer
   */
  public removeTrainer() {
    // Set trainer to null trainer
    this._trainer = nullTrainer;
  }

  public error(): string {
    return this._error;
  }
  /**
   * checks if a trainer has a pokemon with a specific name
   * @param name - name of pokemon
   * @returns
   */
  public hasPokemon(name: string): boolean {
    return this._trainer.pokemon.includes(name);
  }
  /**
   *  updates current trainer
   */
  public saveTrainer() {
    console.log(this._trainer);
    this.updateTrainer(this._trainer);
  }
}
