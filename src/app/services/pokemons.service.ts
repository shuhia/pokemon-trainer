import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Pokemon } from './../models/pokemon.model';
import { BrowserStorageService } from './browser-storage.service';
import { map } from 'rxjs';
import { PokeApiPokemonResult, PokeApiResponse } from '../models/pokeApi';

const BASE_URL = 'https://pokeapi.co/api/v2/pokemon';
// Makes sure that this object is a singleton.
@Injectable({
  providedIn: 'root',
})

// Used to get and provide contacts to Application
export class PokemonService {
  private _pokeApiResponse?: PokeApiResponse;
  private _pokemonNames?: string[];
  private _pokemons: Pokemon[] = [];
  private _error: string = '';

  // DI - Dependency Injection
  constructor(
    private readonly http: HttpClient,
    private readonly storage: BrowserStorageService
  ) {}
  /**
   * Fetches pokemon names from sessionStorage or PokeApi
   * @param limit - amount of pokemon names to get
   * @param offset - where to fetch pokemons
   * @returns {Observable}
   */
  public fetchPokemons(limit = 20, offset = 20) {
    // Reset pokemons
    this._pokemons = [];
    // Returns an observable
    const url = BASE_URL + `?limit=${limit}&offset=${offset}`;
    // If there are pokemons in session storage load pokemons from session storage
    const pokeApiResults = this.storage.getSession(url) as PokeApiResponse;
    if (pokeApiResults) {
      this._pokeApiResponse = pokeApiResults;
      setTimeout(
        () =>
          (this._pokemonNames = pokeApiResults.results.map((pokemon) => {
            return pokemon.name;
          })),
        1
      );
      return null;
    } else {
      return this.http.get<PokeApiResponse>(url).subscribe({
        next: (pokeApiResults: PokeApiResponse) => {
          // Save pokeApi response for future requests
          this.storage.setSession(url, pokeApiResults);
          this._pokeApiResponse = pokeApiResults;
          this._pokemonNames = pokeApiResults.results.map((pokemon) => {
            return pokemon.name;
          });
        },
        error: (error: HttpErrorResponse) => {
          this._error = error.message;
        },
        complete: () => {
          console.log('has fetched pokemons');
        },
      });
    }
  }
  /**
   * Fetches pokemon from PokeApi
   * @param name - name of pokemon
   * @returns {Observable<Pokemon>}
   */
  public fetchPokemon(name: string) {
    const path = '/' + name;
    const url = BASE_URL + path;
    return this.http.get<PokeApiPokemonResult>(url).pipe(
      map((response) => {
        const { id, name, sprites, stats, types } = response;
        const avatar = sprites.front_default;
        return { id, name, avatar, isCaught: false, stats, types } as Pokemon;
      })
    );
  }

  public pokemonNames(): string[] | undefined {
    return this._pokemonNames;
  }

  public pokemons(): Pokemon[] {
    if (!this._pokemons) {
      this.fetchPokemons();
    }
    return this._pokemons;
  }

  public getPokemonByName(name: string): Pokemon | undefined {
    return this._pokemons.find((pokemon) => pokemon.name === name);
  }
  /**
   * Adds pokemon to collection and localStorage
   * @param pokemon
   */
  public addPokemon(pokemon: Pokemon) {
    this._pokemons.push(pokemon);
    this.storage.setLocal('pokemons', this._pokemons);
  }

  public error(): string {
    return this._error;
  }
}
