import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { PokemonTrainerService } from './pokemon-trainer.service';

@Injectable({
  providedIn: 'root',
})

/**
 * This component is used for authentication. It redirects user to correct page depending on if there is a user.
 */
export class AuthService implements CanActivate {
  constructor(
    private trainerService: PokemonTrainerService,
    private router: Router
  ) {}
    
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | boolean
    | UrlTree
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree> {
    if (this.trainerService.hasTrainer()) {
      return true;
    } else {
      this.router.navigateByUrl('/');
      return false;
    }
  }

  /**
   * Logins user by fetching trainer from trainerService
   *
   * @param username - name of user
   */
  login(username: string) {
    this.trainerService.fetchTrainer(username).then(() => {
      this.router.navigateByUrl('/catalogue');
    });
  }

  /**
   * Removes trainer and redirect to login page
   */

  logout() {
    this.trainerService.removeTrainer();
    this.router.navigateByUrl('/');
  }
  /**
   * @return {boolean} returns true if trainer serivce has a user
   */

  get hasUser() {
    return this.trainerService.hasTrainer();
  }
}
