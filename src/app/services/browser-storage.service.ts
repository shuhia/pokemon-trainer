import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})

/**
 * This class wraps localstorage and sessionStorage. It provides get, set and clear method. Get and set method uses JSON methods to convert to and from json object
 */
export class BrowserStorageService {
  /**
   * set localStorage
   * @param key
   * @param value
   */
  public setLocal(key: string, value: Object): void {
    localStorage.setItem(key, JSON.stringify(value));
  }
  /**
   * Get object from local storage
   * @param key
   * @returns {object}
   */
  public getLocal(key: string): Object {
    return JSON.parse(localStorage.getItem(key) || 'null');
  }
  /**
   * Removes a record from localStorage
   * @param key
   */
  public removeLocal(key: string): void {
    localStorage.removeItem(key);
  }
  /**
   * Clears localStorage
   */
  public clearLocal() {
    localStorage.clear();
  }
  /**
   * set sessionStorage
   * @param key
   * @param value
   */
  public setSession(key: string, value: object): void {
    sessionStorage.setItem(key, JSON.stringify(value));
  }
  /**
   * Get object from sessionStorage
   * @param key
   * @returns
   */
  public getSession(key: string) {
    return JSON.parse(sessionStorage.getItem(key) || 'null');
  }
  /**
   * Removes sessionStorage object with key
   * @param key
   */
  public removeSession(key: string): void {
    sessionStorage.removeItem(key);
  }
  /**
   * Clears sessionStorage
   */
  public clearSession() {
    sessionStorage.clear();
  }
}
