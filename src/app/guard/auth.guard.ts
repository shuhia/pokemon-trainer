import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { PokemonTrainerService } from '../services/pokemon-trainer.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    private trainerService: PokemonTrainerService,
    private router: Router
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | boolean
    | UrlTree
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree> {
    if (this.trainerService.hasTrainer()) {
      return true;
    } else {
      this.router.navigateByUrl('/');
      return false;
    }
  }

  logout() {
    localStorage.clear();
    this.router.navigateByUrl('/');
  }
}
