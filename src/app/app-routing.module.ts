import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guard/auth.guard';
import { LoginComponent } from './views/login/login.component';
import { TrainerComponent } from './views/trainer/trainer.component';
import { CatalogueComponent } from './views/catalogue/catalogue.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: LoginComponent,
  },

  {
    path: 'catalogue',
    component: CatalogueComponent,
    canActivate: [AuthGuard],
  },

  {
    path: 'trainer',
    component: TrainerComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
