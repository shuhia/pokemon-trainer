import { Component, OnInit, Input } from '@angular/core';
import { Pokemon as IPokemon } from '../../models/pokemon.model';
import { PokemonService } from '../../services/pokemons.service';
import { PokemonTrainerService } from '../../services/pokemon-trainer.service';
import { PokemonTrainer } from '../../models/pokemon-trainer.model';

// Tells the role of the file. In this case this file is a component
@Component({
  selector: 'app-pokemon-table',
  templateUrl: './pokemon-table.component.html',
  styleUrls: ['./pokemon-table.component.css'],
}) // Decorators
export class PokemonTableComponent implements OnInit {
  constructor(
    private readonly pokemonService: PokemonService,
    private readonly pokemonTrainerService: PokemonTrainerService
  ) {}

  selectedPokemon?: IPokemon;
  @Input() filterFor: string = 'catalog';
  @Input() pokemonNames?: string[];

  ngOnInit(): void {
    this.pokemonService.fetchPokemons();
  }
  trainerPokemons: IPokemon[] = [];
  get pokemonsNames(): string[] | undefined {
    return this.pokemonNames;
  }

  get trainer(): PokemonTrainer {
    return this.pokemonTrainerService.pokemonTrainer();
  }
  public selectPokemon(pokemon: IPokemon): void {
    this.selectedPokemon = pokemon;
  }
}
