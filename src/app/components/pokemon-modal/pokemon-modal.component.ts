import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonTrainerService } from 'src/app/services/pokemon-trainer.service';
@Component({
  selector: '[pokemon-modal]',
  templateUrl: './pokemon-modal.component.html',
  styleUrls: ['./pokemon-modal.component.css'],
})
export class PokemonModalComponent implements OnInit {
  constructor(private readonly pokemonTrainerService: PokemonTrainerService) {}
  @Input() pokemon?: Pokemon;
  ngOnInit(): void {}
  public isCaught(name: string) {
    return this.pokemonTrainerService.hasPokemon(name);
  }
  public handleCatch(name: string): void {
    console.log('catch:' + name);
    this.pokemonTrainerService.catchPokemon(name);
  }

  public handleRelease(name: string): void {
    console.log('Release: ' + name);
    this.pokemonTrainerService.releasePokemon(name);
  }
}
