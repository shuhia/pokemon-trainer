import { Component, OnInit, Input } from '@angular/core';
import { PokemonTrainerService } from '../../services/pokemon-trainer.service';
import { Pokemon } from '../../models/pokemon.model';
import { PokemonService } from '../../services/pokemons.service';
import { BrowserStorageService } from '../../services/browser-storage.service';

@Component({
  selector: '[my-tr]',
  templateUrl: './pokemon-table-row.component.html',
  styleUrls: ['./pokemon-table-row.component.css'],
})
export class PokemonTableRowComponent implements OnInit {
  // Inputs
  pokemon?: Pokemon;
  @Input() name?: string;
  @Input() selectPokemon?: (pokemon: Pokemon) => void;
  constructor(
    private readonly pokemonTrainerService: PokemonTrainerService,
    private readonly pokemonService: PokemonService,
    private readonly storage: BrowserStorageService
  ) {}
  ngOnInit() {
    this.loadPokemon();
  }
  public loadPokemon() {
    if (this.name) {
      // Attempt to load pokemon from session storage
      this.pokemon = this.storage.getSession('pokemon:' + this.name);
      // Fetch pokemon
      if (!this.pokemon)
        this.pokemonService.fetchPokemon(this.name).subscribe({
          next: (pokemon) => {
            this.pokemon = pokemon;
            this.storage.setSession('pokemon:' + this.name, pokemon);
          },
        });
    }
  }

  public isCaught(name: string) {
    return this.pokemonTrainerService.hasPokemon(name);
  }

  public handleCatch(name: string): void {
    console.log('catch:' + name);
    this.pokemonTrainerService.catchPokemon(name);
  }
  public handleRelease(name: string): void {
    console.log('Release: ' + name);
    this.pokemonTrainerService.releasePokemon(name);
  }
}
