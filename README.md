# Pokemon Trainer

Link to Demo [Pokémon Trainer](https://radiant-coast-26349.herokuapp.com/)

[Requirements doc](./AngularPokemonTrainer.pdf)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.1.

## Component Feature Tree

[Figma component tree (.pdf)](./pokemonFigma.pdf)

## Installation

1. Clone the repo

```sh
git clone git@gitlab.com:shuhia/pokemon-trainer.git
```

2. Enter the cloned project folder

```sh
cd pokemon-trainer
```

3. Install npm dependencies

```sh
npm install
```

4. Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Usage

How to use the application

1. Fill in a username and press login
2. In the navbar on top of the page, you can navigate to the different pages of the application.
3. In the Catalogue page, you can watch the catalogue of the generation 1 pokemons. And add them to your collection by catching them.
4. On your trainer page you can see your collected pokemons and release them into the wild.
5. You can logout from the application by clicking logout in the navbar.

## Contributors & Contact

[Alex on](https://www.linkedin.com/in/alex-on-0a08b8107/)

[Christopher Vestman](https://www.linkedin.com/in/christophervestman)

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
